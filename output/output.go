package output

import (
	"fmt"
	"github.com/common-nighthawk/go-figure"
	"github.com/eben/sort/constants"
)

func Greeting() {
	fmt.Println("-----------------------------------------------------------------")
	greet := figure.NewColorFigure(constants.GREETING, "", "blue", true)
	greet.Print()
	fmt.Println("-----------------------------------------------------------------")
	fmt.Println("Sort using three method Application")
	fmt.Println("-----------------------------------------------------------------")
}

func Choose() {
	fmt.Println(constants.MAKE_CHOICE)
	fmt.Println(constants.MERGE)
	fmt.Println(constants.INSERTION)
	fmt.Println(constants.SELECTION)
	fmt.Print("your choice is: ")

}

func InsertNumbers() {
	fmt.Println("-----------------------------------------------------------------")
	fmt.Println(constants.INSERT)
}

func Ending() {
	fmt.Println("-----------------------------------------------------------------")
	end := figure.NewColorFigure(constants.END, "", "red", true)
	end.Print()
}
