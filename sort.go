package main

import (
	"fmt"
	"github.com/eben/sort/constants"
	"github.com/eben/sort/input"
	"github.com/eben/sort/output"
	"github.com/eben/sort/sorts"
)

func main() {
	//start
	output.Greeting()

	// choose sort kind
	output.Choose()
	sortKind, err := input.UserChoice()

	if err != nil {
		fmt.Println(sortKind)
		return
	}

	//get numbers
	output.InsertNumbers()
	numbers, err := input.GetNumbers()

	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("fetching numbers failed. Exiting!")
		return
	}

	var sortedNumbers []int
	if sortKind == constants.MERGE {
		sortedNumbers = sorts.Merge(numbers)
	} else if sortKind == constants.SELECTION {
		sortedNumbers = sorts.Selection(numbers)
	} else {
		sortedNumbers = sorts.Insertion(numbers)
	}
	//do Sorts
	fmt.Print("Your sorted list is: ")
	fmt.Println(sortedNumbers)
	//end
	output.Ending()
}
