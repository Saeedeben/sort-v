package constants

const (
	GREETING           = "Welcome!"
	END                = "BYE!"
	MAKE_CHOICE        = "Choose your kind of sort:"
	MERGE              = "1) Merge Sort"
	INSERTION          = "2) Insertion sort"
	SELECTION          = "3) selection sort"
	INSERT             = "Now please Enter numbers with ',' separator "
	USER_CHOICE_FAILED = "Fetching user choice failed. Exiting!"
)
