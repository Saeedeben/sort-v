package input

import (
	"bufio"
	"fmt"
	"github.com/eben/sort/constants"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func UserChoice() (string, error) {
	var fault error

	userChoice, err := reader.ReadString('\n')

	if err != nil {
		fmt.Println("Fetching user choice failed. Exiting!")
		return "", fault
	}

	userChoice = strings.Replace(userChoice, "\n", "", -1)
	cleanedUserChoice, err := strconv.ParseInt(userChoice, 0, 64)

	switch cleanedUserChoice {
	case 1:
		return constants.MERGE, nil
	case 2:
		return constants.INSERTION, nil
	case 3:
		return constants.SELECTION, nil
	default:
		return constants.USER_CHOICE_FAILED, fault
	}
}

func GetNumbers() ([]int, error) {
	userInput, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Fetching numbers failed. Exiting!")
		return nil, err
	}

	userInput = strings.Replace(userInput, "\n", "", -1)
	var numbersString = strings.Split(userInput, ",")

	var numbers []int
	for _, val := range numbersString {
		cleanedVal, er := strconv.ParseInt(val, 0, 0)
		if cleanedVal == 0 {
			return nil, er
		}
		numbers = append(numbers, int(cleanedVal))
	}

	return numbers, nil
}
