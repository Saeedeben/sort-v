package sorts

func Merge(numbers []int) []int {
	if len(numbers) == 1 {
		return numbers
	}

	divide := len(numbers) / 2
	left := Merge(numbers[divide:])
	right := Merge(numbers[:divide])
	return mergeHelper(left, right)
}

func mergeHelper(left []int, right []int) []int {
	var res []int
	for len(left) > 0 && len(right) > 0 {
		if left[0] > right[0] {
			res = append(res, right[0])
			right = right[1:]
		} else {
			res = append(res, left[0])
			left = left[1:]
		}
	}

	for len(left) > 0 {
		res = append(res, left[0])
		left = left[1:]
	}
	for len(right) > 0 {
		res = append(res, right[0])
		right = right[1:]
	}

	return res
}

func Selection(numbers []int) []int {
	var sorted []int
	iterate := numbers
	for i := 0; i < len(iterate); i++ {
		little := numbers[0]
		pos := 0
		for index, val := range numbers {
			if index != 0 && val < little {
				little = val
				pos = index
			}
		}
		numbers = append(numbers[:pos], numbers[pos+1:]...)
		sorted = append(sorted, little)
	}

	return sorted
}

func Insertion(numbers []int) []int {
	for i := 0; i < len(numbers); i++ {
		j := i
		for j > 0 {
			if numbers[j-1] > numbers[j] {
				numbers[j-1], numbers[j] = numbers[j], numbers[j-1]
			}
			j = j - 1
		}
	}

	return numbers
}

// {5,4,2,6,1}
